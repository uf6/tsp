package com.stevenmacdiarmid.tsp;
/***************************************************************************
 *
 *  @file 	 	NearestNeighbour.java
 *
 *  @brief 	 	Creates a basic nearest-neighbour TSP tour.
 *
 *  @details 	Creates a basic Hamiltonian circuit from a TSPLIB problem
 *              set by starting at a given node and iteratively visiting
 *              its nearest neighbour without revisiting any nodes.
 *
 *  @author 	Steven MacDiarmid
 *  			Edinburgh Napier University
 *  Student ID: 40187076
 *
 *  Course:		BEng (Hons) Software Engineering
 *  Module:		SET09117: Algorithms and Data Structures
 *
 *  @date		04/11/2016
 *
 *  @bug 		None identified.
 *
 *  @copyright	2016, Steven MacDiarmid
 *
 *
 ***************************************************************************/


import java.awt.geom.Point2D;
import java.util.ArrayList;

public class NearestNeighbour {
    ArrayList<Point2D> cities = new ArrayList<>();
    ArrayList<Point2D> result = new ArrayList<>();

    Point2D currentCity;
    Point2D closest;
    double distance;
    private long duration;

    public NearestNeighbour(ArrayList<Point2D> allCities) {
        cities.addAll(allCities);
    }
    public ArrayList<Point2D> solve() {
        long start = System.nanoTime();

        currentCity = cities.get(0);

        while (cities.size() > 0) {


            result.add(currentCity);
            distance = Double.MAX_VALUE;

            for (Point2D city : cities) {
                if (city.distance(currentCity) < distance) {
                    closest = city;
                    distance = city.distance(currentCity);
                }
            }
            currentCity = closest;
            cities.remove(closest);
        }
        duration = (System.nanoTime() - start);
        return result;
    }

    public long getDuration() {
        return duration;
    }
}