package com.stevenmacdiarmid.tsp;
/***************************************************************************
 *
 *  @file 	 	Opt2.java
 *
 *  @brief 	 	Creates a shorter tour by optimizing a nearest-neighbour
 *              sorted TSP problem set using a simplified 2-opt optimization
 *              heuristic.
 *
 *  @details 	Algorithm divides problem set into sets of 4 adjacent nodes
 *              (3 edges) p1.. p4. For every set of four points the method
 *              then rearranges the edges such that the edges p1->p3 and
 *              p2->p4 are created. The total length of the new grouping
 *              is then measured and compared with the original arrangement.
 *              If it is shorter, it is kept. If longer, it is discarded.
 *
 *              Method produces only modest reductions to nearest-neighbour
 *              tour lengths (typically -0.6% to -2.0) but runs extremely
 *              fast (~15 milliseconds for largest TSPLIB problem set
 *              with 85,900 nodes). Can also be used repetitively on certain
 *              problem sets to yield further small improvements.
 *
 *  @author 	Steven MacDiarmid
 *  			Edinburgh Napier University
 *  Student ID: 40187076
 *
 *  Course:		BEng (Hons) Software Engineering
 *  Module:		SET09117: Algorithms and Data Structures
 *
 *  @date		04/11/2016
 *
 *  @bug 		None identified.
 *
 *  @copyright	2016, Steven MacDiarmid
 *
 *
 ***************************************************************************/


import java.awt.geom.Point2D;
import java.util.*;


public class Opt2 {
    static ArrayList<Point2D> allCities = new ArrayList<>();
    static ArrayList<Point2D> result = new ArrayList<>();

    private static long startTime;
    private static long duration;

    public static long getDuration() {
        return duration;
    }

    public Opt2(ArrayList<Point2D> cities) {
        allCities.addAll(cities);
    }

    public ArrayList<Point2D> solve(int iterations) {
        startTime = System.nanoTime();

        int numberOfPairs = allCities.size() - 3;
        int pair;
        while (iterations > 0) {
            for (pair = 0; pair < numberOfPairs; pair++) {
                Point2D p1 = allCities.get(pair);
                Point2D p2 = allCities.get(pair + 1);
                Point2D p3 = allCities.get(pair + 2);
                Point2D p4 = allCities.get(pair + 3);

                double cost1 = p1.distance(p2);
                cost1 += p3.distance(p4);

                double cost2 = p1.distance(p3);
                cost2 += p2.distance(p4);

                if (cost2 < cost1) {
//                Collections.swap(allCities, pair + 1, pair + 2);
                    Point2D temp = allCities.get(pair + 1);
                    allCities.set(pair + 1, allCities.get(pair + 2));
                    allCities.set(pair + 2, temp);
                }
            }
            iterations--;
        }
        duration = (System.nanoTime() - startTime);
        return allCities;
    }
}