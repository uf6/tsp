package com.stevenmacdiarmid.tsp;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

/***************************************************************************
 *
 *  @file 	 	Surface.java
 *
 *  @brief 	 	Extends JPanel to provide a drawing Surface to render a TSP
 *              path consisting of straight edges.
 *
 *  @details 	Creates a surface which takes drawLine commands as a series
 *              of joined X and Y values. Also provides transformm() methods to
 *              remap and scale tour visualisation to the viewport
 *
 *  @author 	Steven MacDiarmid
 *  			Edinburgh Napier University
 *  Student ID: 40187076
 *
 *  Course:		BEng (Hons) Software Engineering
 *  Module:		SET09117: Algorithms and Data Structures
 *
 *  @date		02/11/2016
 *
 *  @bug 		None identified.
 *
 *  @copyright	2016, Steven MacDiarmid
 *
 *
 ***************************************************************************/


public class Surface extends JPanel {
    private void drawRoute(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        /*
        array transform will hold 3 values to control graphics transformation:
        0 - shift of X coordinate
        1 - shift of y coordinate
        2 - scale factor
         */

        double[] transform = transform();
        double xShift = transform[0];
        double yShift = transform[1];
        double scale = (transform[2]);

        g2.setPaint(Color.BLACK);
        g2.fillRect(0,0,GWindow.WIDTH,GWindow.HEIGHT);

        g2.setPaint(Color.yellow);
        Point2D startPoint = GWindow.allPts.get(0);
        Point2D endPoint = GWindow.allPts.get(GWindow.allPts.size() - 1);

        for (int i = 0; i < GWindow.allPts.size() - 1; i++) {
            int x1 = (int) ((GWindow.allPts.get(i).getX() - xShift) * scale);
            int y1 = (int) ((GWindow.allPts.get(i).getY() - yShift) * scale);
            int x2 = (int) ((GWindow.allPts.get(i + 1).getX() - xShift) * scale);
            int y2 = (int) ((GWindow.allPts.get(i + 1).getY() - yShift) * scale);

            g.drawLine(x1,y1,x2,y2);
        }

        int endX = (int) ((endPoint.getX() - xShift) * scale);
        int endY = (int) ((endPoint.getY() - yShift) * scale);
        int startX = (int) ((startPoint.getX() - xShift) * scale);
        int startY = (int) ((startPoint.getY() - yShift) * scale);

        g.drawLine(endX, endY, startX, startY);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawRoute(g);
    }

    private double[] transform() {
        ArrayList<Point2D> points = new ArrayList<>(GWindow.allPts);
        double allX[] = new double[points.size()];
        double allY[] = new double[points.size()];
        double padding = 5.0;
        double scale;
        double minX = 0;
        double minY = 0;
        double maxX = 0;
        double maxY = 0;

        // remap origin of tour to within viewport
        int i = 0;
        for (Point2D pt : points) {
            allX[i] = pt.getX();
            allY[i] = pt.getY();
            Arrays.sort(allX);
            Arrays.sort(allY);
            minX = allX[0];
            minY = allY[0];
            maxX = allX[points.size() - 1];
            maxY = allY[points.size() - 1];
        }

        // scale to viewport..
        // First determine largest axis x or y....

        if (maxX > maxY) {
            scale = GWindow.WIDTH / maxX;
        } else {
            scale = GWindow.HEIGHT / maxY;
        }

        double shiftXY[] = {minX + padding, minY + padding, scale};

        return shiftXY;
    }
}