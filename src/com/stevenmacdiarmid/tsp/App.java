package com.stevenmacdiarmid.tsp;
/***************************************************************************
 *
 *  @file 	 	App.java
 *
 *  @brief 	 	Simple Travelling Salesman Problem solver
 *
 *  @details 	Creates static ArrayLists of Point2D to store TSPLIB
 *              instance data from
 *              http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/
 *
 *              Displays FileChooser to select problem
 *              sets and populates list with data from file. Executes
 *              nearest-neighbour search on tour data then optimizes
 *              with a 2-opt search. Displays data.
 *
 *
 *  @author 	Steven MacDiarmid
 *  			Edinburgh Napier University
 *  Student ID: 40187076
 *
 *  Course:		BEng (Hons) Software Engineering
 *  Module:		SET09117: Algorithms and Data Structures
 *
 *  @date		16/11/2016
 *
 *  @bug 		None identified.
 *
 *  @copyright	2016, Steven MacDiarmid
 *
 *
 ***************************************************************************/


import javax.swing.*;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;

public class App {

    private static String fileName;
    static ArrayList<Point2D> originalData = new ArrayList<>();             // Raw data from file
    static ArrayList<Point2D> NNroute = new ArrayList<>();                  // Holds a nearest-neighbout tour
    static ArrayList<Point2D> O2route = new ArrayList<>();                  // Holds a 2-opt optimized tour

    public static void main(String[] args) {

        /****************************************************************************
         *
         * @brief Display file chooser, run search methods, display results
         *
         * @param Void.
         *
         * @return Void.
         *
         ****************************************************************************/

        ////////////////////////////////////// LAUNCH FILE CHOOSER ///////////////////////////////////////////

        /* Get name of TSPLIB problem instance... */
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose a TSPLIB instance...");

        int returnValue = fc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            fileName = f.getAbsolutePath();
        }
        originalData = loadTSPLib.load(fileName);                           // Raw data loaded

        showData(originalData);


        /* Some problem sets yield shorter tours with repeated optimization passes */
        int iterations = Integer.parseInt(JOptionPane.showInputDialog("Number of optimization passes?: "));


        /* First create a nearest-neighbour tour
           then pass the new tour into the 2-opt
           method for optimization
         */
        NearestNeighbour nn = new NearestNeighbour(originalData);
        NNroute = nn.solve();
        Opt2 o2 = new Opt2(NNroute);
        O2route = o2.solve(iterations);

        LaunchWin();
        displayTour(NNroute, O2route, "nearest-neighbour + 2-opt optimised", iterations);
    }

    public static void LaunchWin() {
        GWindow g2 = new GWindow(O2route);
        g2.setVisible(true);
    }

    public static void showData(ArrayList<Point2D> data) {

        int size = data.size();
        double length = Tour.length(originalData);

        String output = "Number of cities: " + size + "\n";
        output += String.format("Length of tour (unoptimized) %.3f units\n", length);
        JOptionPane.showMessageDialog(null, output, "Raw data", JOptionPane.INFORMATION_MESSAGE);
    }

    private static void displayTour(ArrayList<Point2D> NNroute, ArrayList<Point2D> O2route, String method, int iterations) {

        String output = "File: " + fileName + "\n";
        output += ("Number of cities: " + originalData.size() + ".\n");
        String length = String.format("%.3f", Tour.length(NNroute));
        output += "Nearest-neighbour tour length: " + length + " units\n";
        output += "\n-----------------------------------------------------\n\n";
        output += ("Tour length (" + method + "): ");
        double oldLength = Double.parseDouble(length);
        length = String.format("%.3f", Tour.length(O2route));
        output += length + " units.\n";
        length = String.format("%.3f", oldLength - Tour.length(O2route));
        output += "\u0394 distance: " + length + " units  (" +
                Math.round(Double.parseDouble(length) / Tour.length(O2route) * 100) + "%)\n";
        output += "Compute time: " + Opt2.getDuration() + " nanosecs.\n";
        output += "Optimization passes: " + iterations + "\n";
        Boolean theCheck = O2route.containsAll(NNroute);
        output += "\n-----------------------------------------------------\n\n";
        output += "Route valid (nearest neighbour and 2 opt contain same points): " + theCheck + ".\n";
        output += "===========================================\n";

        JOptionPane.showMessageDialog(null, output, "Tour information", JOptionPane.INFORMATION_MESSAGE);
    }
}
