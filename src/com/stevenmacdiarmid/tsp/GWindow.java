package com.stevenmacdiarmid.tsp;
/***************************************************************************
 *
 *  @file 	 	GWindow.java
 *
 *  @brief 	 	Extends JFrame to hold drawing canvas (Surface) and a List
 *              of nodes to be drawn.
 *
 *  @details 	Creates a 16x10 aspect ratio frame with default width of
 *              700 pixels to hold visual representation of a Travelling
 *              Salesman Problem instance.
 *
 *              Constructor takes an ArrayList of Point2D representing
 *              a TSP tour and exposes the tour data to the drawing
 *              Surface.
 *
 *  @author 	Steven MacDiarmid
 *  			Edinburgh Napier University
 *  Student ID: 40187076
 *
 *  Course:		BEng (Hons) Software Engineering
 *  Module:		SET09117: Algorithms and Data Structures
 *
 *  @date		02/11/2016
 *
 *  @bug 		None identified.
 *
 *  @copyright	2016, Steven MacDiarmid
 *
 *
 ***************************************************************************/


import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class GWindow extends JFrame {

    public static final int WIDTH = 700;
    public static final int HEIGHT = WIDTH / 16 * 10;
    static ArrayList<Point2D> allPts = new ArrayList<>();
    static Dimension dim = new Dimension(WIDTH, HEIGHT);
    private final String TITLE = "Travelling Salesman Problem Visualiser";

    public GWindow(ArrayList<Point2D> pts) {
        allPts.addAll(pts);
        initUI();
    }

    private void initUI() {
        setTitle(TITLE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(dim);
        add(new Surface());
    }
}
