package com.stevenmacdiarmid.tsp;
/***************************************************************************
 *
 *  @file 	 	Tour.java
 *
 *  @brief 	 	Calculate length of TSP problem set
 *
 *  @details 	Sums the total distance between all the points in a
 *
 *  @author 	Steven MacDiarmid
 *  			Edinburgh Napier University
 *  Student ID: 40187076
 *
 *  Course:		BEng (Hons) Software Engineering
 *  Module:		SET09117: Algorithms and Data Structures
 *
 *  @date		16/11/2016
 *
 *  @bug 		None identified.
 *
 *  @copyright	2016, Steven MacDiarmid
 *
 *
 ***************************************************************************/

import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Tour {

	public static double length(ArrayList<Point2D> cities) {

		double result = 0;                          // Holds the route length

		/*
		 * Set the previous city to the last city
		 * in the ArrayList as we need to
		 * measure the length of the entire loop
		 */

		Point2D prev = cities.get(cities.size() -1);

		for (Point2D city : cities) {
			result += city.distance(prev);
			prev = city;
		}

		return result;
	}
}
