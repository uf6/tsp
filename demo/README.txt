Extract 'TSP demo.zip’ to local file system.
Double click 'TSP.jar' to launch the program.
Use file picker to navigate to folder containing 'TSP.jar'.
Double click on 'berlin52.tsp' or 'rl5915.tsp' to process files.