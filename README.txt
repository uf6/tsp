Simple Travelling Salesman Problem solver in Java written to fulfil Napier University module SET09117 (Algorithms and Data Structures) coursework requirement.

Processes, solves and displays problem instances from University of Heidelberg's TSPLIB project (http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/)
